/*
 * Copyright (C) 2015 The CyanogenMod Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.qs.tiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.audiofx.AudioEffect;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.android.systemui.R;
import com.android.systemui.qs.QSTile;
import com.android.systemui.qs.QSTileView;
import com.android.systemui.statusbar.policy.KeyguardMonitor;
import com.android.systemui.statusbar.policy.MediaMonitor;
import com.pheelicks.visualizer.AudioData;
import com.pheelicks.visualizer.FFTData;
import com.pheelicks.visualizer.VisualizerView;
import com.pheelicks.visualizer.renderer.Renderer;

public class VisualizerTile extends QSTile<QSTile.State> implements KeyguardMonitor.Callback {

    private static final Intent AUDIO_EFFECTS =
            new Intent(AudioEffect.ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL);

    private KeyguardMonitor mKeyguardMonitor;
    private ImageView mStaticVisualizerIcon;
    private boolean mLinked;
    private boolean mListening;
    private boolean mPowerSaveModeEnabled;

    private MediaMonitor mMediaMonitor;

    private boolean mDestroyed;

    public VisualizerTile(Host host) {
        super(host);
    }

    @Override
    protected State newTileState() {
        return new State();
    }

    @Override
    protected void handleClick() {
        mHost.startSettingsActivity(AUDIO_EFFECTS);
    }

    @Override
    protected void handleLongClick() {
        mHost.startSettingsActivity(AUDIO_EFFECTS);
    }

    @Override
    protected void handleUpdateState(State state, Object arg) {
        state.icon = ResourceIcon.get(R.drawable.ic_qs_visualizer_static);
        state.visible = true;
        state.label = mContext.getString(R.string.quick_settings_visualizer_label);
        state.contentDescription = mContext.getString(
                R.string.accessibility_quick_settings_visualizer);

        if (!mDestroyed) {
            mUiHandler.post(mUpdateVisibilities);
        }
    }

    @Override
    public void setListening(boolean listening) {
        if (mListening == listening) return;
        mListening = listening;
        // refresh state is called by QSPanel right after calling into this.
    }

    @Override
    protected void handleDestroy() {
        mDestroyed = true;
        mMediaMonitor.setListening(false);
        mMediaMonitor = null;
        super.handleDestroy();

        mKeyguardMonitor.removeCallback(this);
    }

    @Override
    public void onKeyguardChanged() {
        refreshState();
    }

    private final Runnable mRefreshStateRunnable = new Runnable() {
        @Override
        public void run() {
            if (mListening) {
                refreshState();
            }
        }
    };

    private final Runnable mUpdateVisibilities = new Runnable() {
        @Override
        public void run() {
            boolean showVz = mMediaMonitor != null
                    && mMediaMonitor.isAnythingPlaying()
                    && !mKeyguardMonitor.isShowing();
        }
    };
}
