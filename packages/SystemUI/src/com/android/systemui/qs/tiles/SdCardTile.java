package com.android.systemui.qs.tiles;

import com.android.systemui.R;
import com.android.systemui.qs.QSDetailItemsList;
import com.android.systemui.qs.QSTile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.StatFs;
import android.os.storage.IMountService;
import android.os.storage.StorageEventListener;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import cyanogenmod.app.StatusBarPanelCustomTile;

/**
 * Copyright 2015 MaxMustermann2.0
 *
 * Licensed under the Apache License,Version2.0(the"License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an"AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

public class SdCardTile extends QSTile<QSTile.BooleanState> {
    private BroadcastReceiver mExternalStorageReceiver;
    private IMountService mMountService = IMountService.Stub
            .asInterface(ServiceManager.getService("mount"));
    private StorageManager mStorageManager;
    private StorageEventListener mStorageEventListener;
    private String mLabel;
    private long mSize = 0;
    private long mFreeSpace = 0;
    private long mUsedSpace;
    private String mPath;
    private BroadcastReceiver mReceiver;

    public SdCardTile(Host host) {
        super(host);
    }

    @Override
    protected BooleanState newTileState() {
        return new BooleanState();
    }

    @Override
    protected void handleClick() {
        mState.value = !mState.value;
        if (!mState.value) {
            try {
                Toast.makeText(mContext, R.string.sdcard_unmounting, Toast.LENGTH_SHORT).show();
                mMountService.unmountVolume(mPath, true, false);
            } catch (RemoteException e) {
                Toast.makeText(mContext, R.string.sdcard_unmount_failed, Toast.LENGTH_SHORT).show();
                mState.value = true;
            }
        } else {
            try {
                mMountService.mountVolume(mPath);
            } catch (RemoteException e) {
                Toast.makeText(mContext, R.string.sdcard_mount_failed, Toast.LENGTH_SHORT).show();
                mState.value = false;
            }
        }
        refreshState();
    }

    @Override
    protected void handleLongClick() {
        showDetail(true);
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {
        state.visible = true;
        state.value = isExternalSdCardMounted() && mSize > 0;
        if (state.value) {
            state.icon = ResourceIcon.get(R.drawable.ic_qs_sdcard_on);
            NumberFormat nf = new DecimalFormat("#.##");
            state.label = nf.format(mUsedSpace / 1000000000.0) + " GB/" +
                    nf.format(mSize / 1000000000.0) + " GB";
        } else {
            state.icon = ResourceIcon.get(R.drawable.ic_qs_sdcard_off);
            state.label = mContext.getString(R.string.no_sdcard);
        }
    }

    @Override
    public void setListening(boolean listening) {
        if (listening) {
            if (mStorageManager == null) mStorageManager = StorageManager.from(mContext);
            if(mStorageEventListener == null)mStorageEventListener = new StorageEventListener() {
                @Override
                public void onStorageStateChanged(String path, String oldState, String newState) {
                    super.onStorageStateChanged(path, oldState, newState);
                    refreshState();
                }
            };
            mStorageManager.registerListener(mStorageEventListener);
        } else {
            if (mStorageManager != null && mStorageEventListener != null) mStorageManager
                    .unregisterListener(mStorageEventListener);
        }

    }

    @Override
    protected String composeChangeAnnouncement() {
        return mContext.getString(R.string.sdcard);
    }

    @Override
    public DetailAdapter getDetailAdapter() {
        return new SdCardDetailAdapter();
    }

    private boolean isExternalSdCardMounted() {
        try {
            StorageVolume[] volumes = mMountService.getVolumeList();
            for (StorageVolume volume : volumes) {
                if (volume.isRemovable()) {
                    mPath = volume.getPath();
                    Log.d(TAG, mPath);
                    mLabel = volume.getUserLabel();
                    updateSize();
                    return true;
                }
            }
        } catch (RemoteException e) {
            //ignore
        }
        return false;
    }

    private void updateSize() {
        StatFs stat = new StatFs(mPath);
        long blockSize = stat.getBlockSizeLong();
        long blockCount = stat.getBlockCountLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        mSize = blockCount * blockSize;
        mFreeSpace = availableBlocks * blockSize;
        mUsedSpace = mSize - mFreeSpace;
    }

    private final class SdCardDetailAdapter implements DetailAdapter {
        @Override
        public int getTitle() {
            return R.string.sdcard;
        }

        @Override
        public Boolean getToggleState() {
            return mState.value;
        }

        @Override
        public void setToggleState(boolean state) {
            mState.value = state;
            if (!mState.value) {
                try {
                    mMountService.unmountVolume(mPath, true, false);
                } catch (RemoteException e) {
                    Toast.makeText(mContext, R.string.sdcard_unmount_failed, Toast.LENGTH_SHORT).show();
                    mState.value = true;
                }
            } else {
                try {
                    mMountService.mountVolume(mPath);
                } catch (RemoteException e) {
                    Toast.makeText(mContext, R.string.sdcard_mount_failed, Toast.LENGTH_SHORT).show();
                    mState.value = false;
                }
            }
            fireToggleStateChanged(state);
        }

        @Override
        public View createDetailView(Context context, View convertView, ViewGroup parent) {
            if (mState.value) {
                final SdCardDetailView v = (SdCardDetailView)
                        LayoutInflater.from(mContext).inflate(R.layout.sd_card_info,
                                parent, false);
                v.bindData(mFreeSpace, mUsedSpace, mSize, mLabel, mPath);
                return v;
            } else {
                QSDetailItemsList mItems = QSDetailItemsList
                        .convertOrInflate(context, convertView, parent);
                mItems.setEmptyState(R.drawable.ic_qs_sdcard_off, R.string.no_sdcard);
                return mItems;
            }
        }

        @Override
        public Intent getSettingsIntent() {
            return new Intent("android.settings.MEMORY_CARD_SETTINGS");
        }

        @Override
        public StatusBarPanelCustomTile getCustomTile() {
            return null;
        }

    }
}
