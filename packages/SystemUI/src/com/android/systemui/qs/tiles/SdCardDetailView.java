package com.android.systemui.qs.tiles;


import com.android.systemui.R;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class SdCardDetailView extends LinearLayout {
    private ProgressBar mUsageBar;
    private TextView mUsedSpace;
    private TextView mFreeSpace;
    private TextView mMountPoint;
    private TextView mLabel;

    public SdCardDetailView(Context context) {
        super(context);
        setOrientation(LinearLayout.VERTICAL);
    }

    public SdCardDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayout.VERTICAL);
    }

    public SdCardDetailView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOrientation(LinearLayout.VERTICAL);
    }

    private void initViews() {
        mUsageBar = (ProgressBar) findViewById(R.id.sd_card_usage);
        mUsedSpace = (TextView) findViewById(R.id.sd_card_used_space);
        mFreeSpace = (TextView) findViewById(R.id.sd_card_free_space);
        mMountPoint = (TextView) findViewById(R.id.sd_card_mount_point);
        mLabel = (TextView) findViewById(R.id.sd_card_label);
    }

    public void bindData(long freeSpace, long usedSpace, long totalSpace, String label,
                         String mountPoint) {
        initViews();
        if (label == null || label.isEmpty()) label = getContext()
                .getString(R.string.untitled_sdcard);
        mLabel.setText(label);
        mUsageBar.setMax(100);
        mUsageBar.setProgress((int) (usedSpace / (totalSpace * 1.0) * 100));
        NumberFormat nf = new DecimalFormat("#.##");
        mUsedSpace.setText(nf.format(usedSpace / 1000000000.0) + " GB/" +
                nf.format(totalSpace / 1000000000.0) + " GB");
        mFreeSpace.setText(nf.format(freeSpace / 1000000000.0) + " GB " + getContext()
                .getString(R.string.sdcard_free));
        mMountPoint.setText(mountPoint);
    }

}
