package com.android.systemui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SystemUIRestartReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("AndroidRuntime", "We will throw a RuntimeException, this is to force SystemUI" +
                " restart. You can ignore this.");
        throw new RuntimeException("You may ignore this exception; it's thrown to force SystemUI" +
                " restart");
    }
}
